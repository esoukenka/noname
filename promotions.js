$(document).ready(function () {

	$("#promos_btn").click(function () {

		var propertyID = $("#propertyListSelect").val();

		$.ajax({
			url: 'http://terminal2.expedia.com:80/x/lps/promotions/v1/hotels/' + propertyID + '/promos',
			success: function (data) {

				$("#output").html("");
				var great_html = "";
				for (var i in data.Entity) {
					var object = data.Entity[i];
					var form_id = "promo_update_" + i;
					var promoId = object.id;
					great_html += formCreate(form_id, object, "Update Promo " + promoId);
				}
				$("#propertymanagement_result").html(great_html);

				for (var i in data.Entity) {
					var object = data.Entity[i];
					var form_id = "promo_update_" + i;
					$("#" + form_id).submit(function () {
						var formDataObj = $("#" + form_id).serializeObject();
						var formDataString = JSON.stringify(formDataObj);
						$.ajax({
							type: "PUT",
							url: 'http://terminal2.expedia.com:80/x/lps/promotions/v1/hotels/' + propertyID + '/promos/' + object.id,
							data: formDataString,
							success: function (data) {
								$("#output").html("UPDATED SUCCESSFULLY");
							},
							error: function (jqXHR, textStatus, errorThrown) {
								$("#output").html('ERROR: ' + jqXHR.responseText + " - " + textStatus + " - " + JSON.stringify(errorThrown));
							}
						});
						return false;
					});
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$("#output").html('ERROR: ' + jqXHR.responseText + " - " + textStatus + " - " + JSON.stringify(errorThrown));
			}
		});
		return false;
	});
});
