var apiKEY, host;
apiKEY = "xfYm9wE6PRDXy4GaBWSA6wMEhLZrNIBh";
host = "http://terminal2.expedia.com";

$(document).ready(function () {
	"use strict";
	var apiKEY, host;
	apiKEY = "xfYm9wE6PRDXy4GaBWSA6wMEhLZrNIBh";
	host = "http://terminal2.expedia.com";
	$.ajaxSetup({
		type: "GET",
		headers: {
			'Authorization': 'expedia-apikey key=' + apiKEY
		},
		dataType: 'json',
		contentType: 'application/json'
	});

	$("#room_details_btn").click(function () {
		var propertyID = $("#propertyListSelect").val();
		$.ajax({
			url: host + '/x/lps/products/v1/properties/' + propertyID + '/roomTypes',
			success: function (data) {

				$("#output").html("");
				$("#history_insights").hide();
				$("#history_missing").hide();

				var great_html = "";
				for (var i in data.entity) {
					var object = data.entity[i];
					var form_id = "prop_update_" + i;
					var room_id = object.resourceId;

					great_html += formCreate(form_id, object, "Update Room " + room_id);
				}

				$("#propertymanagement_result").html(great_html);

				// fill the room select options
				listRoomTypes(data);
				$("#rate_plan_btn").show();

				for (var i in data.entity) {
					var object = data.entity[i];
					var form_id = "prop_update_" + i;
					var room_id = object.resourceId;

					// room values update
					$("#" + form_id).submit(function () {
						var formDataObj = $("#" + form_id).serializeObject();
						var formDataString = JSON.stringify(formDataObj);
						$.ajax({
							type: "PUT",
							url: host + '/x/lps/products/v1/properties/' + propertyID + '/roomTypes/' + room_id,
							data: formDataString,
							success: function (data) {
								$("#output").html("UPDATED SUCCESSFULLY");
							},
							error: function (jqXHR, textStatus, errorThrown) {
								$("#output").html('ERROR: ' + jqXHR.responseText + " - " + textStatus + " - " + JSON.stringify(errorThrown));
							}
						});
						return false;
					});
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$("#output").html('ERROR: ' + jqXHR.responseText + " - " + textStatus + " - " + JSON.stringify(errorThrown));
			}
		});
		return false;
	});

	$("#rate_plan_btn").click(function () {
		var propertyID = $("#propertyListSelect").val();
		var roomTypeID = $("#roomListSelect").val();

		$.ajax({
			url: host + '/x/lps/products/v1/properties/' + propertyID + '/roomTypes/' + roomTypeID + '/ratePlans',
			data: {
				'status': 'active'
			},
			success: function (data) {

				$("#output").html("");
				$("#history_compression").hide();
				$("#history_missing").hide();

				var great_html = "";
				for (var i in data.entity) {
					var object = data.entity[i];
					var form_id = "prop_update_" + i;
					var rate_plan_id = object.resourceId;

					great_html += formCreate(form_id, object, "Update RatePlan " + rate_plan_id);
				}

				$("#propertymanagement_result").html(great_html);

				for (var i in data.entity) {
					var object = data.entity[i];
					var form_id = "prop_update_" + i;
					var rate_plan_id = object.resourceId;

					// rate plan values update
					$("#" + form_id).submit(function () {
						var formDataObj = $("#" + form_id).serializeObject();
						var formDataString = JSON.stringify(formDataObj);
						$.ajax({
							type: "PUT",
							url: host + '/x/lps/products/v1/properties/' + propertyID + '/roomTypes/' + roomTypeID + '/ratePlans/' + rate_plan_id,
							data: formDataString,
							success: function (data) {
								$("#output").html("UPDATED SUCCESSFULLY");
							},
							error: function (jqXHR, textStatus, errorThrown) {
								$("#output").html('ERROR: ' + jqXHR.responseText + " - " + textStatus + " - " + JSON.stringify(errorThrown));
							}
						});
						return false;
					});
				}
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$("#output").html('ERROR: ' + jqXHR.responseText + " - " + textStatus + " - " + JSON.stringify(errorThrown));
			}
		});
		return false;
	});

	$("#prop_details_btn").click(function () {
		var propertyID = $("#propertyListSelect").val();
		$.ajax({
			url: host + '/x/lps/properties/v1/test/hackathon-' + propertyID,
			data: {
				hotelId: propertyID
			},
			success: function (data) {

				$("#output").html("");
				$("#history_compression").hide();
				$("#history_missing").hide();

				$("#propertymanagement_result").html(formCreate("prop_update", data, "Update!"));

				// property values update
				$("#prop_update").submit(function () {
					var formDataObj = $("#prop_update").serializeObject();
					var formDataString = JSON.stringify(formDataObj.entity);
					$.ajax({
						type: "POST",
						url: host + '/x/lps/properties/v1/test',
						data: '[' + formDataString + ']',
						success: function (data) {
							$("#output").html("UPDATED SUCCESSFULLY");
						},
						error: function (jqXHR, textStatus, errorThrown) {
							$("#output").html('ERROR: ' + jqXHR.responseText + " - " + textStatus + " - " + JSON.stringify(errorThrown));
						}
					});
					return false;
				});
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$("#output").html('ERROR: ' + jqXHR.responseText + " - " + textStatus + " - " + JSON.stringify(errorThrown));
			}
		});
		return false;
	});

	$("#insights_compression_btn").click(function () {
		
		$("#history_missing").hide();
		$("#missed_chart").hide();
		
		var propertyID = "1780110";
		var apiKEY = "9RF7DJj7JCd7gEU2VOwIlhhRlkGVxjiR";
		$.ajax({
			url: host + '/x/mp/insights/compression/' + propertyID,
			headers: {
				'Authorization': 'expedia-apikey key=' + apiKEY
			},
			success: function (data) {
				$("#compression_chart").show();
				drawCompressionChart(data.data.compressionPercent, new Date(), "compression_chart");
				$("#history_compression").show();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$("#output").html('ERROR: ' + jqXHR.responseText + " - " + textStatus + " - " + JSON.stringify(errorThrown));
			}
		});
		return false;
	});

	$("#week_compression_btn").click(function () {
		var today = new Date();
		var last_week = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7, 0, 0, 0, 0);

		var compressionWeek = new Array();
		compressionWeek[0] = Math.floor((Math.random() * 70) + 50);
		compressionWeek[1] = Math.floor((Math.random() * 70) + 50);
		compressionWeek[2] = Math.floor((Math.random() * 70) + 50);
		compressionWeek[3] = Math.floor((Math.random() * 70) + 50);
		compressionWeek[4] = Math.floor((Math.random() * 70) + 50);

		drawCompressionChart(compressionWeek, last_week, "compression_chart");
		return false;
	});

	$("#month_compression_btn").click(function () {
		var today = new Date();
		var last_week = new Date(today.getFullYear(), today.getMonth() - 1, today.getDate(), 0, 0, 0, 0);

		var compressionWeek = new Array();
		compressionWeek[0] = Math.floor((Math.random() * 70) + 50);
		compressionWeek[1] = Math.floor((Math.random() * 70) + 50);
		compressionWeek[2] = Math.floor((Math.random() * 70) + 50);
		compressionWeek[3] = Math.floor((Math.random() * 70) + 50);
		compressionWeek[4] = Math.floor((Math.random() * 70) + 50);

		drawCompressionChart(compressionWeek, last_week, "compression_chart");
		return false;
	});

	$("#year_compression_btn").click(function () {
		var today = new Date();
		var last_week = new Date(today.getFullYear() - 1, today.getMonth(), today.getDate(), 0, 0, 0, 0);

		var compressionWeek = new Array();
		compressionWeek[0] = Math.floor((Math.random() * 70) + 50);
		compressionWeek[1] = Math.floor((Math.random() * 70) + 50);
		compressionWeek[2] = Math.floor((Math.random() * 70) + 50);
		compressionWeek[3] = Math.floor((Math.random() * 70) + 50);
		compressionWeek[4] = Math.floor((Math.random() * 70) + 50);

		drawCompressionChart(compressionWeek, last_week, "compression_chart");
		return false;
	});


	$("#insights_missed_btn").click(function () {
		
		$("#history_compression").hide();
		$("#compression_chart").hide();
		
		var propertyID = "1780110";
		var apiKEY = "9RF7DJj7JCd7gEU2VOwIlhhRlkGVxjiR";
		$.ajax({
			url: host + '/x/mp/insights/missedOpportunitiesToday/' + propertyID,
			headers: {
				'Authorization': 'expedia-apikey key=' + apiKEY
			},
			success: function (data) {
				$("#missed_chart").show();
				drawMissedChart(data.data.missedOpportunities, "missed_chart");
				$("#history_missing").show();
			},
			error: function (jqXHR, textStatus, errorThrown) {
				$("#output").html('ERROR: ' + jqXHR.responseText + " - " + textStatus + " - " + JSON.stringify(errorThrown));
			}
		});
		return false;
	});

	$("#week_missed_btn").click(function () {
		var missedWeek = new Array();
		missedWeek[0] = { 
			hotelName: "Crown Inn",
			bookings: Math.floor((Math.random() * 50) + 10)
		};
		missedWeek[1] = { 
			hotelName: "Sorrento Hotel",
			bookings: Math.floor((Math.random() * 50) + 10)
		};
		missedWeek[2] = { 
			hotelName: "8th Avenue Condos",
			bookings: Math.floor((Math.random() * 50) + 10)
		};

		drawMissedChart(missedWeek, "missed_chart");
		return false;
	});

	$("#month_missed_btn").click(function () {
		var missedMonth = new Array();
		missedMonth[0] = { 
			hotelName: "Crown Inn",
			bookings: Math.floor((Math.random() * 50) + 10)
		};
		missedMonth[1] = { 
			hotelName: "Sorrento Hotel",
			bookings: Math.floor((Math.random() * 50) + 10)
		};
		missedMonth[2] = { 
			hotelName: "8th Avenue Condos",
			bookings: Math.floor((Math.random() * 50) + 10)
		};

		drawMissedChart(missedMonth, "missed_chart");
		return false;
	});

	$("#year_missed_btn").click(function () {
		var missedYear = new Array();
		missedYear[0] = { 
			hotelName: "Crown Inn",
			bookings: Math.floor((Math.random() * 50) + 10)
		};
		missedYear[1] = { 
			hotelName: "Sorrento Hotel",
			bookings: Math.floor((Math.random() * 50) + 10)
		};
		missedYear[2] = { 
			hotelName: "8th Avenue Condos",
			bookings: Math.floor((Math.random() * 50) + 10)
		};

		drawMissedChart(missedYear, "missed_chart");
		return false;
	});


	listProperties();
	$("#rate_plan_btn").hide();
	$("#history_compression").hide();
	$("#history_missing").hide();
	$("#missed_chart").hide();
	$("#compression_chart").hide();
});

function listProperties() {
	var apiKEY = "xfYm9wE6PRDXy4GaBWSA6wMEhLZrNIBh";
	$.ajax({
		type: "GET",
		url: 'http://terminal2.expedia.com/x/lps/products/v1/properties',
		data: {
			'status': 'active',
			'offset': '0',
			'limit': '20'
		},
		success: function (data) {
			var x = '<select id="propertyListSelect" style="width:210px">';
			for (var i in data.entity) {
				x += '<option value="' + data.entity[i].resourceId + '">' + data.entity[i].name + '</option>';
			}
			x += '</select>';
			$('#propertyList_results').html(x);
		},
		error: function (jqXHR, textStatus, errorThrown) {
			alert('Error: ' + textStatus + " - " + JSON.stringify(errorThrown));
		}
	});
}

function formCreate(id, content, button_label) {
	var ret_str = "<div class='form-group'>" +
			"<form class='form-inline' method='POST' action='' id='" + id + "'>";
	ret_str += traverse(content, print, "", 0);
	if (button_label !== "") {
		ret_str += "<input type='submit' class='btn btn-primary' value='" + button_label + "'/>";
	}
	ret_str += "</form>" +
			"</ div>";
	return ret_str;
}

function listRoomTypes(data) {
	var selectHtml = '<select id="roomListSelect">';
	for (var i in data.entity) {
		selectHtml += '<option value="' + data.entity[i].resourceId + '">' + data.entity[i].partnerCode + '</option>';
	}
	selectHtml += '</select>';

	$('#roomList_result').html(selectHtml);
}

function traverse(o, func, cumulative_key, level) {
	var ret_val = "";
	for (var i in o) {
		var new_key = (cumulative_key === "") ? i : (cumulative_key + "[" + i + "]");
		if ((o[i] !== null) && (typeof (o[i]) === "object")) {
			ret_val += printTitle(i, level);
			ret_val += traverse(o[i], func, new_key, level + 1);
		} else {
			ret_val += func.apply(this, [i, new_key, o[i], level + 1]);
		}
	}
	return ret_val;
}

function print(label, key, value, indent) {
	return getSpace(indent) + "<label style='inline-block;width:150px;text-align:right;padding-right:10px'>" + label + "</label> <input class='form-control' style='width:300px' name='" + key + "' value='" + ((value !== null) ? value : "") + "' /></br>";
}

function printTitle(label, indent) {
	return "<b\>" + getSpace(indent) + label + "</b></br>";
}

function getSpace(level) {
	var space = "";
	if (level !== 0) {
		space += "<div style='display:inline-block;width:10px'/>";//"|_";
	}
	for (var i = 0; i < level; i++) {
		space += "<div style='display:inline-block;width:10px'/>";//"_";
	}

	return space;
}

function drawCompressionChart(percents, today, canvasID) {

	var options = {
		//Boolean - Show a backdrop to the scale label
		scaleShowLabelBackdrop: true,
		//String - The colour of the label backdrop
		scaleBackdropColor: "rgba(255,255,255,0.75)",
		// Boolean - Whether the scale should begin at zero
		scaleBeginAtZero: true,
		//Number - The backdrop padding above & below the label in pixels
		scaleBackdropPaddingY: 2,
		//Number - The backdrop padding to the side of the label in pixels
		scaleBackdropPaddingX: 2,
		//Boolean - Show line for each value in the scale
		scaleShowLine: true,
		//Boolean - Stroke a line around each segment in the chart
		segmentShowStroke: true,
		//String - The colour of the stroke on each segement.
		segmentStrokeColor: "#fff",
		//Number - The width of the stroke value in pixels
		segmentStrokeWidth: 2,
		//Number - Amount of animation steps
		animationSteps: 100,
		//String - Animation easing effect.
		animationEasing: "easeOutBounce",
		//Boolean - Whether to animate the rotation of the chart
		animateRotate: true,
		//Boolean - Whether to animate scaling the chart from the centre
		animateScale: false,
		//String - A legend template
		legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
	};

	var colors = [
		{
			color: "#F7464A",
			highlight: "#FF5A5E"
		},
		{
			color: "#46BFBD",
			highlight: "#5AD3D1"
		},
		{
			color: "#FDB45C",
			highlight: "#FFC870"
		},
		{
			color: "#D8BFD8",
			highlight: "#EE82EE"
		},
		{
			color: "#32CD32",
			highlight: "#00FF00"
		}
	];

	var data = new Array();
	var counter = 0;
	for (var i in percents) {
		var value = percents[i];
		var date_value = new Date(today.getFullYear(), today.getMonth(), today.getDate() + counter, 0, 0, 0, 0);
		data[i] = {
			value: value,
			color: colors[i].color,
			highlight: colors[i].highlight,
			label: date_value.toDateString()
		};
		counter++;
	}

	var ctx = document.getElementById(canvasID).getContext("2d");
	new Chart(ctx).Doughnut(data, options);
}

function drawMissedChart(opportunities, canvasID) {

	var options = {
		//Boolean - Show a backdrop to the scale label
		scaleShowLabelBackdrop: true,
		//String - The colour of the label backdrop
		scaleBackdropColor: "rgba(255,255,255,0.75)",
		// Boolean - Whether the scale should begin at zero
		scaleBeginAtZero: true,
		//Number - The backdrop padding above & below the label in pixels
		scaleBackdropPaddingY: 2,
		//Number - The backdrop padding to the side of the label in pixels
		scaleBackdropPaddingX: 2,
		//Boolean - Show line for each value in the scale
		scaleShowLine: true,
		//Boolean - Stroke a line around each segment in the chart
		segmentShowStroke: true,
		//String - The colour of the stroke on each segement.
		segmentStrokeColor: "#fff",
		//Number - The width of the stroke value in pixels
		segmentStrokeWidth: 2,
		//Number - Amount of animation steps
		animationSteps: 100,
		//String - Animation easing effect.
		animationEasing: "easeOutBounce",
		//Boolean - Whether to animate the rotation of the chart
		animateRotate: true,
		//Boolean - Whether to animate scaling the chart from the centre
		animateScale: false,
		//String - A legend template
		legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
	};

	var labels = new Array();
	var data_set = new Array();

	var counter = 0;
	for (var i in opportunities) {
		var object = opportunities[i];

		labels[counter] = object.hotelName;
		data_set[counter] = object.bookings;

		counter++;
	}

	var data = {
		labels: labels,
		datasets: [
			{
				fillColor: "rgba(151,187,205,0.2)",
				strokeColor: "rgba(151,187,205,1)",
				pointColor: "rgba(151,187,205,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(151,187,205,1)",
				data: data_set
			}
		]
	};

	var ctx = document.getElementById(canvasID).getContext("2d");
	new Chart(ctx).Radar(data, options);
}

